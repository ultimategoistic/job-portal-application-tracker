## JobPortalApplicationTracker

This is a Django application that helps users track their job applications. It allows them to:

* **Store application details:** Company name, job title, application date, etc.
* **Search applications:** Search across various fields like company name and job title.
* **View application details:** Get a consolidated view of all their applications.

**Features:**

* User authentication and authorization
* Search functionality across multiple fields
* Table-based view of applications
* Responsive design
* User Forgot Password and Reset Password

**Prerequisites:**

* Python 3.x
* Django framework
* A database (e.g., PostgreSQL, MySQL)

**Installation:**

1. Clone this repository:

   ```bash
   git clone https://gitlab.com/ultimategoistic/job-portal-application-tracker.git
   ```

2. Create a virtual environment and activate it (recommended):

   ```bash
   python -m venv venv
   source venv/bin/activate
   ```

3. Install dependencies:

   ```bash
   pip install -r requirements.txt
   ```

4. Create a database and configure Django settings:

   * Edit `settings.py` to configure your database connection details.
   * Set the `SECRET_KEY` using environment variables (recommended).
   * Also set `EMAIL_BACKEND` to send email for password reset.

5. Apply database migrations:

   ```bash
   python manage.py migrate
   ```

6. Run the development server:

   ```bash
   python manage.py runserver
   ```

**Usage:**

1. Access the application in your web browser (usually `http://localhost:8000/`).
2. Create an account or log in if you already have one.
3. Add your job application details.
4. Search for applications using the search bar.
5. View details of your saved applications.
