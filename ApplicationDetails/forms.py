from django import forms
from .models import JobApplication

class JobApplicationForm(forms.Form):
    class Meta:
        model = JobApplication
        fields = ['date', 'portal_name', 'job_title', 'company_name', 'contact_details', 'application_link', 'remarks']
