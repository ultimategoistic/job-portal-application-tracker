from django.shortcuts import render, redirect
from .models import JobApplication
from .forms import JobApplicationForm
from django.db import models
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from datetime import date
from django.urls import reverse_lazy, reverse
from django.db.models import Q
# Create your views here.

class ApplicationListView(ListView):
    model = JobApplication
    context_object_name = 'application_counts'
    template_name = 'ApplicationDetails/application_list.html'
    paginate_by = 10

    def get_queryset(self):
        user = self.request.user
        return super().get_queryset().filter(user=user).values('date').annotate(count=models.Count('id')).order_by('-date')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not context['application_counts']:  # Check if any data exists
            context['no_data'] = True  # Add a flag for no data
        return context


class ApplicationDetailListView(ListView):
    model = JobApplication
    context_object_name = 'applications'
    template_name = 'ApplicationDetails/application_detail_list.html'
    paginate_by = 5

    def get_queryset(self):
        user = self.request.user
        date_str = self.kwargs.get('date')
        search_field = self.request.GET.get('search_field')

        if date_str:
            try:
                selected_date = date.fromisoformat(date_str)
                queryset = self.model.objects.filter(date=selected_date)
            except ValueError:
                queryset = self.model.objects.none()
        else:
            queryset = self.model.objects.all()

        if user.is_authenticated:
            queryset = queryset.filter(user=user)

        if search_field:
            # Search across multiple fields using Q objects
            queryset = queryset.filter(
                Q(company_name__icontains=search_field) | Q(job_title__icontains=search_field) | Q(portal_name__icontains=search_field) | Q(job_location__icontains=search_field)
            )

        return queryset.order_by('-id')

class ApplicationCreateView(CreateView):
    model = JobApplication
    fields = ['date', 'portal_name', 'job_title', 'company_name', 'job_location', 'contact_details', 'application_link', 'remarks']
    template_name = 'ApplicationDetails/add_job_application.html'
    
    def get_success_url(self):
        updated_object = self.object
        return reverse_lazy('ApplicationDetails:application_details', kwargs={'date': updated_object.date, 'application_id': updated_object.id})
    def form_valid(self,form):
        form.instance.user = self.request.user
        return super().form_valid(form)

class ApplicationDetailView(DetailView):
    model = JobApplication
    template_name = 'ApplicationDetails/application_details.html'
    pk_url_kwarg = 'application_id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Add any additional context data specific to the application detail
        return context

    def get_queryset(self):
        user = self.request.user
        date_str = self.kwargs.get('date')
        if date_str:
            try:
                selected_date = date.fromisoformat(date_str)
                queryset = self.model.objects.filter(date=selected_date)
            except ValueError:
                queryset = self.model.objects.none()
        else:
            queryset = self.model.objects.all()

        if user.is_authenticated:
            queryset = queryset.filter(user=user)
        return queryset

class ApplicationUpdateView(UpdateView):
    model = JobApplication
    fields = ['date', 'portal_name', 'company_name', 'job_location', 'contact_details', 'application_link', 'remarks']
    template_name = 'ApplicationDetails/edit_application.html'
    pk_url_kwarg = 'application_id'

    def get_success_url(self):
        return reverse_lazy('ApplicationDetails:application_details', kwargs={'date': self.object.date, 'application_id': self.object.pk})

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)    

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['jobapplication'] = self.object
        return context

class ApplicationDeleteView(DeleteView):
    model = JobApplication
    template_name = 'ApplicationDetails/delete_application.html'
    pk_url_kwarg = 'application_id'

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)
    
    def get_success_url(self):
        return reverse_lazy('ApplicationDetails:application_detail_list', kwargs={'date': self.object.date})