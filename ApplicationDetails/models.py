from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from datetime import date
# Create your models here.

class JobApplication(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE,default=1,)
    date = models.DateField(verbose_name="Applied Date")
    portal_name = models.CharField(max_length=100, verbose_name="Portal's Name")
    job_title = models.CharField(max_length=200, verbose_name="Job Title")
    company_name = models.CharField(max_length=200, verbose_name="Company Name")
    job_location = models.CharField(max_length=300, blank=True, verbose_name="Job Location")
    contact_details = models.TextField(blank=True, verbose_name="Company Contact Details")
    application_link = models.URLField(verbose_name="Link to Job Application")
    remarks = models.TextField(verbose_name="Remarks")
    
    def get_absolute_url(self):
        return reverse("ApplicationDetails:application_details", kwargs={"application_id": self.id})

    def __str__(self):
        return f"{self.job_title} at {self.company_name}"