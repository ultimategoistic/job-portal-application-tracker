from django.urls import path
from . import views
from django.contrib.auth.decorators import login_required

app_name = "ApplicationDetails"

urlpatterns = [
    path('', login_required(views.ApplicationListView.as_view()), name='application_list'),
    path('applications/<date>/', login_required(views.ApplicationDetailListView.as_view()), name='application_detail_list'),
    path('applications/<date>/<int:application_id>/', login_required(views.ApplicationDetailView.as_view()), name='application_details'),
    path('add_application_details/', login_required(views.ApplicationCreateView.as_view()), name='add_job_application'),
    path('applications/<int:application_id>/update/', login_required(views.ApplicationUpdateView.as_view()), name='edit_application'),
    path('applications/<int:application_id>/delete/', login_required(views.ApplicationDeleteView.as_view()), name='delete_application'),
]
