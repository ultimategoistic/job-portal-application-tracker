from django.urls import path
from . import views
from django.contrib.auth import views as authentication_views

app_name = "users"
urlpatterns = [
    path("register/", views.register, name="register"),
    path("profile/", views.profilepage, name="profile"),
    path("update_profile", views.profile_details, name="profile_details"),
]
