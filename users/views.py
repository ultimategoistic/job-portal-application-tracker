from django.shortcuts import render, redirect
from .forms import RegisterForm, ProfileForm
from django.contrib import messages
from django.contrib.auth.views import PasswordChangeView
from django.contrib.auth.decorators import login_required
# Create your views here.

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Welcome {username.capitalize()}, Your account is created, Now you can Log In to your account.')
            return redirect('login')
    else:
        form = RegisterForm()
    return render(request, "users/register.html", { "form" : form })

@login_required
def profilepage(request):
    return render(request, "users/profile.html")


@login_required
def profile_details(request):
    if request.method == 'POST':
        profile_form = ProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if profile_form.is_valid():
            profile_form.save()
            messages.success(request, 'Your profile has been updated successfully!')
            return redirect('users:profile_details')
    else:
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, "users/profile_details.html", {'profile_form': profile_form})